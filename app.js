const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const lessMiddleware = require('less-middleware');

const bookInfo = require('./routes/bookInfo');
const cat = require('./routes/cat');
const searchResult = require('./routes/searchResult');
const chapter = require('./routes/chapter');
const hotSearchWords = require('./routes/hotSearchWords');
const searchRecommend = require('./routes/searchRecommend');
const catDetail = require('./routes/catDetail');
const categories = require('./routes/categories');
const sort = require('./routes/sort');

const app = express();
app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By", ' 3.2.1')
    res.header("Content-Type", "application/json;charset=utf-8");
    next()
});
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(lessMiddleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/bookInfo', bookInfo);
app.use('/searchResult', searchResult);
app.use('/cat', cat);
app.use('/chapter', chapter);
app.use('/hotSearchWords', hotSearchWords);
app.use('/searchRecommend', searchRecommend);
app.use('/catDetail', catDetail);
app.use('/categories', categories);
app.use('/sort', sort);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;