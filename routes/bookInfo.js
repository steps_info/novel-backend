/**
 * Created by pan on 2018年02月07日14:25:44.
 * 搜索小说，请求全本小说PC接口
 * @url 小说链接
 */

const express = require('express');
const router = express.Router();
const superagent = require('superagent');
const cheerio = require("cheerio");
require('superagent-charset')(superagent);

router.get('/', function (req, res, next) {
    if (req.query.url) {
        superagent.get(req.query.url).charset("gbk").end(function (err, response) {
            if (err) {
                res.send({
                    result: 0,
                    msg: err
                });
                next();
            } else {
                let chapters = [];
                let $ = cheerio.load(response.text);
                let $bookdetail = $("#bookdetail");
                let lists = $(".zjlist").find("a");

                lists.each(function (key, el) {
                    chapters.push({
                        title: $(el).text(),
                        url: req.query.url + $(el).attr("href")
                    })
                });
                res.send({
                    result: 1,
                    data: {
                        imgUrl: $bookdetail.find("img").attr("src"),
                        title: $bookdetail.find("h1").text(),
                        author: $("#info").find("span").eq(0).text(),
                        updatetime: $("#info").find("span").eq(2).text().replace(/\\n|\s/ig, ""),
                        newchapter: $("#info").find("span").eq(3).text().replace(/\\n|\s/ig, ""),
                        intro: $("#intro").text().replace(/\\n|\s/ig, ""),
                        chapters: chapters
                    }
                })
            }
        })


    } else {
        res.send({
            result: 0,
            msg: "非法参数"
        })
    }
});

module.exports = router;