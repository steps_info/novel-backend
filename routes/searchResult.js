/**
 * Created by pan on 2018年02月07日14:25:52.
 * 搜索小说，请求全本小说PC接口
 * @keywords 小说名称，需encode
 */
const express = require('express');
const router = express.Router();
const superagent = require('superagent');
const cheerio = require("cheerio");

/* GET book listing. */
router.get('/', function (req, res, next) {
    if (req.query.text) {
        superagent.get('http://zhannei.baidu.com/cse/search?q=' + encodeURIComponent(req.query.text) + '&entry=1&s=1657774807603928942&srt=def&nsid=0').end(function (err, response) {
            if (err) {
                res.send({
                    result: 0,
                    msg: err
                });
                next();
            } else {
                let result = [];
                let $ = cheerio.load(response.text);
                let $lists = $(".result-item");
                $lists.each(function (k, el) {
                    let $el = $(el);
                    result.push({
                        url: $el.find("a").attr("href"),
                        title: $el.find(".result-item-title").text().replace(/\\n|\s/ig, ""),
                        author: $el.find(".result-game-item-info-tag").eq(0).text().replace(/\\n|\s/ig, ""),
                        type: $el.find(".result-game-item-info-tag").eq(1).text().replace(/\\n|\s/ig, ""),
                        desc: $el.find(".result-game-item-desc").text().replace(/\n/ig, ""),
                        imgUrl: $el.find("img").attr("src")
                    })
                });
                res.send({
                    result: 1,
                    data: result
                })
            }
        })
    } else {
        res.send({
            result: 0,
            msg: "非法参数"
        })
    }
});


module.exports = router;