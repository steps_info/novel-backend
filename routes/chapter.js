/**
 * Created by pan on 2018年02月07日14:25:32.
 * @url 小说章节链接
 */
const express = require('express');
const router = express.Router();
const superagent = require('superagent');
const cheerio = require("cheerio");
require('superagent-charset')(superagent);

router.get('/', function (req, res, next) {
    console.log(req.query.url)
    if (req.query.url) {
        superagent.get(req.query.url).charset("gbk").end(function (err, response) {
            if (err) {
                res.send({
                    result: 0,
                    msg: err
                })
            } else {
                let $ = cheerio.load(response.text);
                res.send({
                    result: 1,
                    title: $("h1").text(),
                    author: $(".read-titlelinke").children("span").text(),
                    data: $("#content").text().replace(/\n\n/ig, "<br>"),
                    prev: $(".previewpage").attr("href"),
                    categories: "http://www.bxwx.io/book/" + req.query.url.match(/\/([^\/]+)\//g)[1].replace(/\//ig, ""),
                    next: $(".nextpage").attr("href")
                })
            }
        })
    } else {
        res.send({
            result: 0,
            msg: "参数错误"
        })
    }

});

module.exports = router;