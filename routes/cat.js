/**
 * Created by pan on 2017/6/22.
 * 小说分类
 * 来源于追书神器
 */

var express = require('express');
var router = express.Router();
var superagent = require('superagent');

router.get('/', function (req, res, next) {
    superagent.get('http://api.zhuishushenqi.com/cats/lv2/statistics').end(function (err, response) {
        if (err) {
            res.send({
                result: 0,
                msg: err
            });
        } else {
            res.send({
                result: 1,
                data: response.body
            })
        }
    })
});

module.exports = router;
