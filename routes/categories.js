/**
 * Created by pan on 2018年02月07日14:25:32.
 * @link 小说目录
 */
const express = require('express');
const router = express.Router();
const superagent = require('superagent');
const cheerio = require("cheerio");

router.get('/', function (req, res, next) {
    if (req.query.link) {
        superagent.get("http://www.quanben.io/" + req.query.link + "/list.html").end(function (err, response) {
            if (err) {
                res.send({
                    result: 0,
                    msg: err
                })
            } else {
                let list = [];
                let $ = cheerio.load(response.text);
                $(".list3").children("li").each(function (k, el) {
                    let $el = $(el);
                    list.push({
                        title: $(el).text(),
                        url: $(el).find("a").attr("href")
                    })
                });
                res.send({
                    result: 1,
                    title: $("h1").text(),
                    data: list
                })
            }
        })
    } else {
        res.send({
            result: 0,
            msg: "参数错误"
        })
    }

});

module.exports = router;