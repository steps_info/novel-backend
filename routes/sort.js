/**
 * Created by pan on 2018年02月07日14:25:32.
 * @id 排行榜id
 */
const express = require('express');
const router = express.Router();
const superagent = require('superagent');

router.get('/', function (req, res, next) {
    console.log(req.query.id)
    if (req.query.id) {
        superagent.get("https://api.zhuishushenqi.com/ranking/" + req.query.id).end(function (err, response) {
            if (err) {
                res.send({
                    result: 0,
                    msg: err
                })
            } else {
                let data=JSON.parse(response.text);
                res.send({
                    result:1,
                    data:data.ranking.books
                })
            }
        })
    } else {
        res.send({
            result: 0,
            msg: "参数错误"
        })
    }

});

module.exports = router;