/**
 * Created by pan on 2017/6/22.
 * 搜索推荐
 * 来源于追书神器
 */

var express = require('express');
var router = express.Router();
var superagent = require('superagent');

router.get('/', function (req, res, next) {
    superagent.get("http://api.zhuishushenqi.com/book/hot-word").end(function (err, response) {
        if (err) {
            res.send({
                result: 0,
                msg: err
            });
        } else {
            res.send({
                result:1,
                data:response.body.hotWords
            })
        }
    })
    
});

module.exports = router;